﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private DataContext _dataContext;

        public EfRepository(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dataContext.Set<T>().ToListAsync();

        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _dataContext.Set<T>().FirstOrDefaultAsync(entity => entity.Id.Equals(id));
        }

        public async Task<IEnumerable<T>> GetByIdRangeAsync(IEnumerable<Guid> ids)
        {
            return await _dataContext.Set<T>().Where(entity => ids.Any(id => entity.Id.Equals(id))).ToListAsync();
        }

        public async Task<T> FirstOrDefault(Expression<Func<T, bool>> expr)
        {
            return await _dataContext.Set<T>().FirstOrDefaultAsync(expr);
        }

        public async Task<IEnumerable<T>> Where(Expression<Func<T, bool>> expr)
        {
            return await _dataContext.Set<T>().Where(expr).ToListAsync();
        }


        public async Task CreateAsync(T entity)
        {
            await _dataContext.Set<T>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            _dataContext.Set<T>().Remove(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            await _dataContext.SaveChangesAsync();
        }

    }
}
