﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<IEnumerable<T>> GetByIdRangeAsync(IEnumerable<Guid> ids);

        Task<T> FirstOrDefault(Expression<Func<T, bool>> expr);

        Task<IEnumerable<T>> Where(Expression<Func<T, bool>> expr);

        Task UpdateAsync(T entity);

        Task CreateAsync(T entity);

        Task DeleteAsync(T entity);
    }
}