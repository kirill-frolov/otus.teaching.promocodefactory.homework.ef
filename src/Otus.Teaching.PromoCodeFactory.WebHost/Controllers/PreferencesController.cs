﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : Controller
    {
        private IRepository<Preference> _prefrencesRepository;

        public PreferencesController(IRepository<Preference> prefrencesRepository)
        {
            _prefrencesRepository = prefrencesRepository;
        }


        /// <summary>
        /// Возвращает список всех предпочтений
        /// </summary>
        /// <returns></returns>

        [HttpGet]   
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            var preferences = await _prefrencesRepository.GetAllAsync();
            var responce = preferences.Select(x => new PreferenceResponse
            {
                Id = x.Id,
                Name = x.Name

            }).ToList();

            return Ok(responce);
        }
    }
}
