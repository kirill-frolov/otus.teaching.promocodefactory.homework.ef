﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private IRepository<Customer> _customerRepository;
        private IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получение списка клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            var customersModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Email = x.Email,

                }).ToList();
            return Ok(customersModelList);
        }
        /// <summary>
        /// Получение клиента по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            return Ok(new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Preferences = customer.Preferences.Select(x => new PreferenceResponse
                {
                    Id = x.PreferenceId,
                    Name = x.Preference.Name
                }).ToList(),
                PromoCodes = customer.Promocodes.Select(p => new PromoCodeShortResponse
                {
                    Id = p.PromocodeId,
                    Code = p.PromoCode.Code,
                    BeginDate = p.PromoCode.BeginDate.ToString(),
                    EndDate = p.PromoCode.EndDate.ToString(),
                    PartnerName = p.PromoCode.PartnerName,
                    ServiceInfo = p.PromoCode.ServiceInfo,
                }).ToList()
            });
        }

        private async Task<IEnumerable<Preference>> GetPreferenceAsync(IEnumerable<Guid> ids)
        {
            return await _preferenceRepository.GetByIdRangeAsync(ids);
        }


        /// <summary>
        /// Создание клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await GetPreferenceAsync(request.PreferenceIds);
            var customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
            };
            customer.Preferences = preferences.Select(x => new CustomerPreference()
            {
                Customer = customer,
                Preference = x
            }).ToList();


            await _customerRepository.CreateAsync(customer);
            return CreatedAtAction(nameof(GetCustomerAsync), new
            {
                id = customer.Id
            }, null);

        }
        /// <summary>
        /// Редактирование клиента
        /// </summary>
        /// <param name="id">идентификатор клиента</param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            var preferences = await GetPreferenceAsync(request.PreferenceIds);
            customer.Preferences= preferences.Select(x => new CustomerPreference()
            {
                Customer = customer,
                Preference = x
            }).ToList();
            await _customerRepository.UpdateAsync(customer);
            return Ok();

        }
        
        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <param name="id">идентификатор клиента</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            await _customerRepository.DeleteAsync(customer);
            return Ok();
        }
    }
}