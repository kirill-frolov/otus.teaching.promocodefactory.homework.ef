﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private IRepository<PromoCode> _promoCodesRepository;
        private IRepository<Customer> _customersRepository;
        private IRepository<Preference> _preferencesRepository;

        public PromocodesController(IRepository<PromoCode> promoCodesRepository,
            IRepository<Customer> customersRepository,
            IRepository<Preference> preferencesRepository
            )
        {
            _promoCodesRepository = promoCodesRepository;
            _customersRepository = customersRepository;
            _preferencesRepository = preferencesRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodesRepository.GetAllAsync();
            var response = promoCodes.Select(x => new PromoCodeShortResponse
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString(),
                EndDate = x.EndDate.ToString(),
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo
            }).ToList();
            return Ok(response);

        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await _preferencesRepository.FirstOrDefault(x => x.Name == request.Preference);
            if (preference == null)
                return BadRequest();

            var customers = await _customersRepository
                .Where(d => d.Preferences.Any(x => x.Preference.Id == preference.Id));
            
            var promoCode = new PromoCode
            {
                Id = Guid.NewGuid(),
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(10),
                Preference = preference,
                PreferenceId = preference.Id
            };
            await _promoCodesRepository.CreateAsync(promoCode);
            foreach (var customer in customers)
            {
                var customerPropmoCode = new CustomerPromoCode
                {
                    Customer = customer,
                    CustomerId = customer.Id,
                    PromoCode = promoCode,
                    PromocodeId = promoCode.Id
                };
                customer.Promocodes.Add(customerPropmoCode);
                await _customersRepository.UpdateAsync(customer);
            };
            return CreatedAtAction(nameof(GetPromocodesAsync), new { }, null);
        }
    }
}